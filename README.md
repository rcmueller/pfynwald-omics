## When trees die: Understanding how plants and microbes interact and influence soil biogeochemical processes

This repository contains detailed description of the omics data processing pipeline in the [Wiki](https://gitlab.com/rcmueller/pfynwald-omics/-/wikis/) including links and references to external software, and tailor-made scripts which can be downloaded here.
